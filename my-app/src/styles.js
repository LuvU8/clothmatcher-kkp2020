export const buttonStyle = () => ({
    position: "relative",
    top: 50,
    marginLeft: 10,
    outline: 0,
    textAlign: "center",
    fontSize: "20px",
    backgroundColor: "lightgrey",
    minWidth: 200,
    height: 50,
    border: "solid 2px black",
    borderRadius: 100,
    borderColor: "black",
    color: "black",
  });

  export const buttonStyleBuy = () => ({
    position: "relative",
    outline: 0,
    textAlign: "center",
    fontSize: "17px",
    backgroundColor: "rgb(199,0,0)",
    width: 250,
    height: 50,
    border: "solid 2px black",
    borderRadius: 10,
    borderColor: "black",
    color: "white",
  });

  export const buttonStyleHeart = () => ({
    position: "relative",
    outline: 0,
    textAlign: "center",
    backgroundColor: "none",
    width: 50,
    height: 50,
    borderColor: "white",
    color: "white",
  });

  export const textStyle = () => ({
    position: "relative",
    outline: 0,
    textAlign: "center",
    fontSize: "20px",
    width: 250,
    height: 50,
    color: "white",
  });