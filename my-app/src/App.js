import React, {Component} from "react";
import 'semantic-ui-css/semantic.min.css';
import "./App.css";
import "./App.scss";
import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from "./components/Nav";

class App extends Component {
render() {
  return (
<div>
  <div className="ui container">
  <Nav align='center'/>
  </div>
</div>
  )
}
}

export default App;
