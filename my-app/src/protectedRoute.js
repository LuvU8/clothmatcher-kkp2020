import React from "react";
import { Route } from "react-router-dom";
import Auth from './components/Auth'

export const ProtectedRoute = ({component: Component, ...rest}) => {
    return (
        <Route {...rest} render={
           (props) => {
            if (Auth.isAuthenticated() == 1){
                return <Component {...props}/>
            } else {
                window.location.assign('/');
            }
            }
           }
        />
    )
}
