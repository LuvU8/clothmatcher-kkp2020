import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Clothes from "./pages/Clothes";
import Shoes from "./pages/Shoes";
import Access from "./pages/Access";
import Product from "./pages/Product";
import Wishlist from "./pages/Wishlist";
import Sales from "./pages/Sales";
import Registration from "./pages/Registration";
import Profile from "./pages/Profile";
import ChangeName from "./pages/ChangeName";
import ChangeEmail from "./pages/ChangeEmail";
import {ProtectedRoute} from './protectedRoute'

const AppRouter = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/clothes" component={Clothes} />
      <Route exact path="/shoes" component={Shoes} />
      <Route exact path="/access" component={Access} />
      <Route path="/product/:name" component={Product} />
      <ProtectedRoute exact path="/wishlist" component={Wishlist} />
      <Route exact path="/sales" component={Sales} />
      <Route exact path="/registration" component={Registration} />
      <ProtectedRoute exact path="/profile" component={Profile} />
      <Route exact path="/changeName" component={ChangeName} />
      <Route exact path="/changeEmail" component={ChangeEmail} />
      <Route exact path="*" component={() => <h1>НУ И КУДА ТЫ ПОЛЕЗ?</h1>} />
    </Switch>
  );
};

export default AppRouter;
