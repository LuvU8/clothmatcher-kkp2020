/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { Header } from 'semantic-ui-react'

export default function Wishlist() {
    return (
        <div>
            <div className="ui container">
                <Header as='h1'>Ваш список желаемого</Header>
                <div className="column">
                    <div className="ui card">
                        <div className="image"></div>
                        <div className="content">
                            <a className="header">Wishlist card</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
