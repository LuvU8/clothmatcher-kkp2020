/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import SearchExampleStandard from '../Search';
import { Container, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import Discounts from "./Discounts";
import "./StylesForPages.css";

export default function Home() {
  return (
    <div className="App">
      <Container id="mainstyles">
        <Header as='h1' align='left'>
          ПОИСК
        </Header>
        <SearchExampleStandard > </SearchExampleStandard>
      </Container>
      <Container>
        <Header as='h1' align='left'>
          ТОП СКИДОК  <Link to="/sales" className="item" className="ui inverted secondary button" id="buttpos">Смотреть все</Link>
        </Header>
      </Container>
      <Discounts />
      <Container className='backcolors intro'>
        <Header as='h1' align='left' className='backcolors intro details'>
          Что такое Clothmatcher?
        </Header>
        <p className="backcolors intro text">
         Наш универсальный сервис по подбору одежды по лучшей цене в интернете. Здесь собраны лучшие сайты и скидки, простая 
         и удобная навигация поможет найти то, что ты так давно искал.
    </p>
      </Container>
      <Container className='backcolors social'>
        <Header as='h1' align='left' className='backcolors social is' >
          Мы в социальных сетях
        </Header>
        <p className="backcolors social text">
          Следи за нашими проектами в социальных сетях.
    </p>
      </Container>
      <Container  className='backcolors out'>
        <p>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
          ligula eget dolor. Aenean massa strong. Cum sociis natoque penatibus et
        </p>
      </Container>
    </div>
  );
}