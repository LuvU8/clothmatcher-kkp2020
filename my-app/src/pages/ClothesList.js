import React, { Component } from 'react'
import PostData from '../json/Products.json';
import { Container } from "semantic-ui-react";
import { Link } from 'react-router-dom'
import { textStyle } from "../styles.js";

class ClothesList extends Component {
    render() {
        return (
            <div>
                <Container  align='center'>
                    <div className="row" align="center">
                        {PostData.map((postDetail) => {
                            if (postDetail.type == 'clo') {
                                return <div className="card">
                                    <img src={process.env.PUBLIC_URL + postDetail.image} alt='loading' />
                                    <h3>{postDetail.price}</h3>
                                    <Link to={{ pathname: '/product/' + postDetail.name }} style={textStyle()} className="ui red button">КУПИТЬ</Link>
                                </div>
                        }})}
                    </div>
                </Container>
            </div>
        )
    }
}

export default ClothesList;
