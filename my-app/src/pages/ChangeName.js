import React, { Component } from 'react'
import { Container, Form, Button, Header } from "semantic-ui-react";
import Axios from 'axios';
import Auth from '../components/Auth'

class ChangeName extends Component {
    constructor(props) {
        super(props);
        this.state = {
          name: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    state = {}
    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmit() {
            Axios.post("http://localhost:3001/api/changeName", {name: this.state.name, email: Auth.getMail()});
            alert('Успешная смена имени!');
            Auth.updateName(this.state.name);
            window.location.assign('/profile');
        }
    render() {
        return (
            <Container>
                <Header as='h1'>Изменение имени</Header>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group widths='equal'>
                        <Form.Input
                            label='Имя'
                            placeholder='Введите новый логин'
                            name='name'
                            value={this.state.name}
                            onChange={this.handleChange}
                            required
                        />
                    </Form.Group>
                    <Button type='submit' color='red'>Изменить имя</Button>
                </Form>
            </Container>
        );
    }
}

export default ChangeName;
