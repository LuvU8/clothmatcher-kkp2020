import React, { Component } from 'react'
import { Container, Form, Button, Header } from "semantic-ui-react";
import Axios from 'axios';
import Auth from '../components/Auth'

class ChangeEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
          email: ''
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    state = {}
    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmit() {
        Axios.get(`http://localhost:3001/api/get/${this.state.email}`).then(response => {
            if (response.data.length == 0){
                Axios.post("http://localhost:3001/api/changeEmail", {emailnew: this.state.email, emailold: Auth.getMail()});
                alert('Успешная смена почты');
                Auth.updateEmail(this.state.email);
                window.location.assign('/profile');
            } else {
                alert('Данный email уже занят! Меняйте на другой!');
            };
        });  
    }
    render() {
        return (
            <Container>
                <Header as='h1'>Изменение почты</Header>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group widths='equal'>
                        <Form.Input
                            label='Почта'
                            placeholder='Введите новую почту'
                            name='email'
                            value={this.state.email}
                            onChange={this.handleChange}
                            required
                        />
                    </Form.Group>
                    <Button type='submit' color='red'>Изменить почту</Button>
                </Form>
            </Container>
        );
    }
}

export default ChangeEmail;