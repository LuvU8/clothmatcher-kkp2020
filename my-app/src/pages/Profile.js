import React from "react";
import Auth from '../components/Auth'
import { Link } from "react-router-dom";

export default function Profile() {
    return (
        <div className="ui container" align="center" id="navext">
        <div className="row">
          <h3>Добро пожаловать, {Auth.getName()}</h3>
          <Link to="/changeName" className="item" className="ui inverted secondary button" id="navbuttons">Изменить имя</Link></div>
          <div className="row">
          <h3>Ваша почта: {Auth.getMail()}</h3>
          <Link to="/changeEmail" className="item" className="ui inverted secondary button" id="navbuttons">Изменить почту</Link>
      </div></div>
    );
  }
  