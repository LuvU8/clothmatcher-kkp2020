import React, { Component } from 'react'
import { Container, Form, Button, Header } from "semantic-ui-react";
import Axios from 'axios';

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    state = {}
    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmit() {
        var pass1 = document.getElementById("pass1");
        var pass2 = document.getElementById("pass2");
        var sogl = document.getElementById("soglasie");
        if (pass1.value.length >= 8 && pass1.value.length <= 30){
            if (pass1.value == pass2.value) {
                if (sogl.checked == true){
                    Axios.get(`http://localhost:3001/api/get/${this.state.email}`).then(response => {
                        if (response.data.length == 0){
                            Axios.post("http://localhost:3001/api/insert", {name: this.state.name, email: this.state.email, password: pass1.value });
                            alert('Успешная регистрация!');
                            window.location.reload();
                        } else {
                            alert('Данный email уже занят! Войдите в приложение под ним или используйте другой...');
                        };
                    }); 
                } else {
                    alert('Вы должны согласиться с нашими правилами, даже если не читали их!');
                }
            } else {
                alert('Изначально введённый пароль не совпадает с повторным. Повторите ввод!');
            }
        } else {
            alert('Пароль не может быть короче 8 символов и длиннее 30!');
        } 
    }

    render() {
        return (
            <Container>
                <Header as='h1'>Регистрация в ClothMatcher</Header>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group widths='equal'>
                        <Form.Input
                            label='Имя'
                            placeholder='Введите логин'
                            name='name'
                            value={this.state.name}
                            onChange={this.handleChange}
                            required
                        />
                        <Form.Input
                            label='Email'
                            placeholder='Введите email'
                            name='email'
                            value={this.state.email}
                            onChange={this.handleChange}
                            required
                        />
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Input
                            id='pass1'
                            label='Пароль'
                            type='password'
                            placeholder='Введите пароль'
                            required/>
                        <Form.Input
                            id='pass2'
                            label='Подтвердждение пароля'
                            type='password'
                            placeholder='Повторите пароль'
                            required/>
                    </Form.Group>
                    <Form.Checkbox id='soglasie' label='Я соглашаюсь с правилами сервиса' required/>
                    <Button type='submit' color='red'>Зарегистрироваться</Button>
                </Form>
            </Container>
        );
    }
}

export default Registration;
