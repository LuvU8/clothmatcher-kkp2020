import React, { Component } from 'react'
import PostData from '../json/Products.json';
import { Link } from 'react-router-dom'
import { textStyle } from "../styles.js";

class SalesList extends Component {
    render() {
        return (
            <div className="ui container">
            <Link to="/" className="item" className="ui inverted secondary button" id="buttpos">Назад</Link>
            <div className="row" align="center" id="skidki">
                {PostData.map((postDetail) => {
                    if (postDetail.type == 'disc'){
                    return <div className="card">
                    <img src={process.env.PUBLIC_URL + postDetail.image} alt='loading' />
                    <h3>{postDetail.price}</h3>
                    <Link to={{ pathname: '/product/' + postDetail.name }} style={textStyle()} className="ui red button">КУПИТЬ</Link>
                    </div> 
                }})}
            </div></div>
        )
    }
}

export default SalesList;