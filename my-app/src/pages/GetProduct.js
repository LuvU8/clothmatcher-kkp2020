import React, { Component } from 'react';
import SizeDropdown from '../components/SizeDropdown';
import { Container, Header, Grid, Image } from 'semantic-ui-react';
import PostData from '../json/Products.json';
import { buttonStyleBuy } from "../styles";
import "./StylesForPages.css";

class GetProduct extends Component {
    render() {
        return (
            <div>
                {PostData.map((postDetail) => {
                    if (postDetail.name.replace(/ /g, '%20') == window.location.href.split('product/').pop()) {
                        return <div className="App" >
                            <Container id="mainstyles">
                                <Grid columns={2} divided align='center'>
                                    <Grid.Column>
                                        <Image src={process.env.PUBLIC_URL + postDetail.image} alt='loading' align='left' />
                                        <Image src={process.env.PUBLIC_URL + postDetail.image} alt='loading' />
                                        <Image src={process.env.PUBLIC_URL + postDetail.image} alt='loading' align='left' />
                                        <Image src={process.env.PUBLIC_URL + postDetail.image} alt='loading' />
                                    </Grid.Column>
                                    <Grid.Column id='centrirovanie1'>
                                        <br></br><br></br><br></br><br></br>
                                        <Header as='h1' align='center' id='centrirovanie'>
                                            {postDetail.name}
                                        </Header>
                                        <br></br>
                                        <h3 align='center'>Цена: {postDetail.price} </h3>
                                        <br></br>
                                        <SizeDropdown align='center' />
                                        <br></br>
                                        <br></br>
                                        <div align='center'>
                                            <button class="transpbut" onClick={()=>alert('Добавлено в желаемое!')}><i className="heart outline link icon big"></i></button>
                                            <a href={postDetail.link} style={buttonStyleBuy()} className="ui red button" >Перейти к магазину</a>
                                        </div>
                                        <br></br><br></br><br></br><br></br>
                                    </Grid.Column>
                                </Grid>
                            </Container>
                        </div>
                    }
                })}
            </div>
        )
    }
}

export default GetProduct;