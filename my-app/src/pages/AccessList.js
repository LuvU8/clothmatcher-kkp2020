import React, { Component } from 'react'
import PostData from '../json/Products.json';
import { Link } from 'react-router-dom'
import { textStyle } from "../styles.js";

class AccessList extends Component {
    render() {
        return (
            <div>
            <div className="ui container">
            <div className="row" align="center" >
                {PostData.map((postDetail) => {
                    if (postDetail.type == 'acc'){
                    return <div className="card">
                    <img src={process.env.PUBLIC_URL + postDetail.image} alt='loading'/>
                    <h3>{postDetail.price}</h3> 
                  <Link to={{ pathname: '/product/' + postDetail.name}} style={textStyle()} className="ui red button">КУПИТЬ</Link>
                  </div>
                }})}
            </div></div></div>
        )
    }
}

export default AccessList;
