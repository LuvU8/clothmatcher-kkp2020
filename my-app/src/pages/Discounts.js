import React, { Component } from 'react'
import PostData from '../json/Discounts.json';
import { Link } from 'react-router-dom'
import { textStyle } from "../styles.js";
import { Container } from 'semantic-ui-react';
import Carousel from 'react-elastic-carousel';

class Discounts extends Component {
    render() {
        const breakPoints = [
            { width: 1, itemsToShow: 1 },
            { width: 500, itemsToShow: 2 },
            { width: 768, itemsToShow: 3 },
            { width: 1200, itemsToShow: 4 },
          ]
        return (
        <Container className='backcolors carsl'>
        <Carousel breakPoints={breakPoints}>
                {PostData.map((postDetail) => {
                        return <div className="card">
                        <img src={process.env.PUBLIC_URL + postDetail.image} alt='loading' />
                        <h3>{postDetail.price}</h3>
                        <Link to={{ pathname: '/product/' + postDetail.name }} style={textStyle()} className="ui red button">КУПИТЬ</Link>
                        </div>   
                })}
            </Carousel>
       </Container>
        )
    }
}

export default Discounts;