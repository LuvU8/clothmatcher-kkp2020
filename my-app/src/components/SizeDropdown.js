import React from 'react'
import { Dropdown } from 'semantic-ui-react'

const options = [
  { key: 1, text: 'S', value: 1 },
  { key: 2, text: 'M', value: 2 },
  { key: 3, text: 'L', value: 3 },
  { key: 3, text: 'XL', value: 3 },
]

const SizeDropdown = () => (
  <Dropdown clearable options={options} selection />
)

export default SizeDropdown
