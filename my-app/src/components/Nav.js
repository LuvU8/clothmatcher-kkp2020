/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { Link } from "react-router-dom";
import { Button } from 'semantic-ui-react'
import "./Navbar.css"
import UserDropdown from "./UserDropdown";

const Nav = () => {
  return (
    <div className="ui container fluid" align="center" id="navext">
      <div className="column" id="navext1"> 
        <div className="ui secondary pointing menu" id="navbarcustom">
          <Link to="/" className="item" className="ui inverted secondary button" id="navbuttons">Домашняя страница </Link>
          <Link to="/clothes" className="item" className="ui inverted secondary button" id="navbuttons">Одежда </Link>
          <Link to="/shoes" className="item" className="ui inverted secondary button" id="navbuttons">Обувь </Link>
          <Link to="/access" className="item" className="ui inverted secondary button" id="navbuttons">Аксессуары </Link>
          <UserDropdown> </UserDropdown>
        </div>

      </div>
      <Button icon='arrow up big' className='arrup' onClick={() => { window.scrollTo(0, 0) }} ></Button>
    </div>
  );
};

export default Nav;