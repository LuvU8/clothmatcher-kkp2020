import React from 'react'
import ModalExampleModal from "./modal";
import { Dropdown } from 'semantic-ui-react'
import { Link } from "react-router-dom";
import Auth from './Auth';

function UserDropdown() {
    if (Auth.isAuthenticated() == 0) {
    return (
        <div className="right menu">
        <Dropdown icon='user outline link icon big'>
        <Dropdown.Menu>
          <Dropdown.Header>Профиль</Dropdown.Header>
          <ModalExampleModal> </ModalExampleModal>
          <Dropdown.Item><Link to="registration">Регистрация</Link></Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown></div>
    ) } else {
        return (
            <div className="right menu">
            <Link to="/wishlist" id="svgbuttons"><i className="heart outline link icon big"></i> </Link>
            <Dropdown icon='user outline link icon big'>
            <Dropdown.Menu>
              <Dropdown.Header>{Auth.getName()}</Dropdown.Header>
              <Dropdown.Item><Link to="profile">Перейти в профиль</Link></Dropdown.Item>
              <Dropdown.Item onClick={Auth.logout}>Выйти</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown></div>
        )
    }
  }
  
  export default UserDropdown