import Cookies from 'js-cookie';

class Auth {
    constructor() {
        this.isAuth = Cookies.get('isAuth');
        this.mail = Cookies.get('mail');
        this.pass = Cookies.get('pass');
        this.name = Cookies.get('Username');
        this.a = 0;
    }

    login(mail, pass, name) {
        require("pidcrypt/seedrandom");
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var cipherAuth = aes.encryptText("1", "194725");
        var cipherMail = aes.encryptText(mail, "194725");
        var cipherPass = aes.encryptText(pass, "194725");
        var cipherName = aes.encryptText(name, "194725");
        Cookies.set('isAuth', cipherAuth);
        Cookies.set('mail', cipherMail);
        Cookies.set('pass', cipherPass);
        Cookies.set('Username', cipherName);
    }

    updateName(name) {
        require("pidcrypt/seedrandom");
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var cipherName = aes.encryptText(name, "194725");
        Cookies.set('Username', cipherName);
    }

    updateEmail(email) {
        require("pidcrypt/seedrandom");
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var cipherEmail = aes.encryptText(email, "194725");
        Cookies.set('mail', cipherEmail);
    }

    logout() {
        Cookies.set('isAuth', '0');
        Cookies.set('mail', '');
        Cookies.set('pass', '');
        Cookies.set('Username', '');
        window.location.reload();
    }

    isAuthenticated(){
        require("pidcrypt/seedrandom")
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var decrypted = aes.decryptText(Cookies.get('isAuth'), "194725");
        return decrypted;
    }

    getMail(){
        require("pidcrypt/seedrandom")
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var decrypted = aes.decryptText(Cookies.get('mail'), "194725");
        return decrypted;
    }

    getPass(){
        require("pidcrypt/seedrandom")
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var decrypted = aes.decryptText(Cookies.get('pass'), "194725");
        return decrypted;
    }

    getName(){
        require("pidcrypt/seedrandom")
        var pidCrypt = require("pidcrypt");
        require("pidcrypt/aes_cbc");
        var aes = new pidCrypt.AES.CBC();
        var decrypted = aes.decryptText(Cookies.get('Username'), "194725");
        return decrypted;
    }
}

export default new Auth();