import React from 'react'
import { Link } from "react-router-dom";
import { Button, Header, Modal, Dropdown, Form } from 'semantic-ui-react'
import "./Navbar.css"
import Axios from 'axios'
import Auth from './Auth'

function signIn(){
    var mail = document.getElementById("mail");
    var pass = document.getElementById("pass");
    Axios.get(`http://localhost:3001/api/get/${mail.value}`).then(response => {
      if (response.data.length == 1){
        Axios.get(`http://localhost:3001/api/get/${mail.value}/${pass.value}`).then(response => {
          if (response.data.length == 1){
            Axios.get(`http://localhost:3001/api/getName/${mail.value}`).then(response => {
              Auth.login(mail.value, pass.value, response.data[0].username);
              alert('Успешный вход!');
              window.location.assign('/profile');
            })
          } else {
            alert('Неверный пароль!');
          }
       });
      } else {
        alert('Пользователь с такой почтой не зарегистрирован! Измените почту или зарегистрируйтесь...');
      };
  }); 
}

function ModalExampleModal() {
  const [open, setOpen] = React.useState(false)
  return (
    <Modal id="modalpos"
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Dropdown.Item>Войти</Dropdown.Item>}
    >
      <Modal.Content >
        <Header as='h1'>Войти в ClothMatcher</Header>
        <Form id='inmodalpos' onSubmit={signIn}>
          <Form.Group widths='equal'>
            <Form.Input
              id='mail'
              label='Email'
              placeholder='Email'
              name='email'
              required/>
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input
            id='pass'
            label='Пароль'
            type='password'
            required/>
          </Form.Group>
          <Button type='submit' color='red'>Войти</Button>
        <Link onClick={() => setOpen(false)} to='registration' color='gray' className="ui gray button">Зарегистрироваться</Link>
        </Form>
      </Modal.Content>
    </Modal>
  )
}

export default ModalExampleModal