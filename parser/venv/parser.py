import requests
from bs4 import BeautifulSoup
import csv
import json

HOST = 'https://www.asos.com/'
URL = 'https://www.asos.com/men/sale/cat/?cid=8409&nlid=mw%7Csale%7Cshop%20sale%20by%20product%7Csale%20view%20all&page='
URL_CLOTHING = 'https://www.asos.com/men/new-in/new-in-clothing/cat/?cid=6993&nlid=mw%7Cclothing%7Cshop%20by%20product%7Cnew%20in&page='
URL_SHOES = 'https://www.asos.com/men/shoes-boots-trainers/cat/?cid=4209&nlid=mw%7Cshoes%7Cshop%20by%20product%7Cview%20all&page='
URL_ACCESSOIRES = 'https://www.asos.com/men/accessories/cat/?cid=4210&nlid=mw%7Caccessories%7Cshop%20by%20product%7Cview%20all&page='
HEADERS = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 YaBrowser/21.2.4.165 Yowser/2.5 Safari/537.36'
}
cards = []
json_products=open('../../my-app/src/json/Products.json','w', encoding='utf-8')
json_disc=open('../../my-app/src/json/Discounts.json','w', encoding='utf-8')
pages = 5

def get_html(url, params=''):
    r = requests.get(url, headers=HEADERS, params=params)
    return r

def get_products(html, type):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('article', class_='_2qG85dG')
    for item in items:
        link = item.find('a', class_='_3TqU78D').get('href')
        html2 = get_html(link)
        soup2 = BeautifulSoup(html2.text, 'html.parser')
        img = soup2.find('div', class_='product-gallery').find('div', class_='product-carousel').find('img').get('src')
        cards.append(
            {
                'type': type,
                'name': item.find('div', class_='_3J74XsK').get_text(),
                'link': link,
                'image': img,
                'price': '£' + item.find('p', class_='_1ldzWib').get_text().split('£')[-1]
            }
        )

def parser():
    for i in range(1):
        print(URL + str(i+1))
        html = get_html(URL + str(i+1))
        if html.status_code == 200:
            get_products(html.text, 'disc')
        else:
            print('чето не работает')
    for i in range(pages):
        print(URL_CLOTHING + str(i+1))
        html = get_html(URL_CLOTHING + str(i+1))
        if html.status_code == 200:
            get_products(html.text, 'clo')
        else:
            print('чето не работает')
    for i in range(pages):
        print(URL_SHOES + str(i+1))
        html = get_html(URL_SHOES + str(i+1))
        if html.status_code == 200:
            get_products(html.text, 'shoe')
        else:
            print('чето не работает')
    for i in range(pages):
        print(URL_ACCESSOIRES + str(i+1))
        html = get_html(URL_ACCESSOIRES + str(i+1))
        if html.status_code == 200:
            get_products(html.text, 'acc')
        else:
            print('чето не работает')
    kek = []
    for i in range(len(cards)):
        for j in range(i + 1, len(cards)):
            if cards[i]["name"] == cards[j]["name"]:
                kek.append(j)
    for i in range(len(kek)):
        cards.pop(kek[i])
    cards_disc = []
    for i in range(len(cards)):
        if cards[i]["type"] == 'disc':
            cards_disc.append(cards[i])
    print(json.dumps(cards_disc, ensure_ascii=False), file=json_disc)
    json_disc.close()
    print(json.dumps(cards, ensure_ascii=False), file=json_products)
    json_products.close()

parser()