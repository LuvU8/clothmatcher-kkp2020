const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');
const cors = require('cors');

 const db = mysql.createPool({
      host: "localhost",
      user: "root",
      password: "password",
      database: "cloth"
  })

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get("/test", (req, res) => {
    const sql = "SELECT * FROM `achievements`;";
    db.query(sql, (err, result) => {
        res.send(result);
    });
});

app.get("/api/get/:email", (req, res) => {
    const useremail = req.params.email;
    const sql = "SELECT useremail FROM users WHERE useremail = ?";
    db.query(sql, useremail, (err, result) => {
        res.send(result);
    });
})

app.get("/api/getName/:email", (req, res) => {
    const useremail = req.params.email;
    const sql = "SELECT username FROM users WHERE useremail = ?";
    db.query(sql, useremail, (err, result) => {
        res.send(result);
    });
})

app.get("/api/get/:email/:password", (req, res) => {
    const useremail = req.params.email;
    const password = req.params.password;
    const sql = "SELECT useremail FROM users WHERE useremail = ? AND password = ?";
    db.query(sql, [useremail, password], (err, result) => {
        res.send(result);
    });
})

app.get("/api/find/:email/:password/:name", (req, res) => {
    const useremail = req.params.email;
    const password = req.params.password;
    const name = req.params.name;
    const sql = "SELECT * FROM users WHERE useremail = ?, password = ? AND username = ?";
    db.query(sql, [useremail, password, name], (err, result) => {
        res.send(result);
    });
})

app.post("/api/insert", (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    
    const sql = "INSERT INTO users (username, useremail, password) VALUES (?, ?, ?)";
    db.query(sql, [name, email, password], (err, result) => {
        console.log(result);
    });
});

app.post("/api/changeName", (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    toString(name);
    toString(email);
    
    const sql = "UPDATE users SET username = ? WHERE useremail = ?";
    db.query(sql, [name, email], (err, result) => {
        console.log(result);
    });
});

app.post("/api/changeEmail", (req, res) => {
    const emailnew = req.body.emailnew;
    const emailold = req.body.emailold;
    toString(emailnew);
    toString(emailold);
    
    const sql = "UPDATE users SET useremail = ? WHERE useremail = ?";
    db.query(sql, [emailnew, emailold], (err, result) => {
        console.log(result);
    });
});

app.listen(3001, () => {
    console.log("running on port 3001");
});